package ua.lokha.demorecoder;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DemoCommandExecutor implements CommandExecutor, TabExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0) {
            sender.sendMessage(
                    "§e=========[demo]=========" +
                            "\n§4/demo reload §7- перезагрузить конфиг" +
                            "\n§4/demo enable [ник]/all §7- начать записывать демки" +
                            "\n§4/demo disable [ник]/all §7- остановить записывать демки" +
                            "\n§4/demo status §7- информация о текущих демках и настройках" +
                            "\n§4/demo list §7- список записанных демок" +
                            "\n§4/demo play [имя демки] §7- посмотреть демку" +
                            "\n§4/demo skip [кол-во секунды] §7- промотать вперед при присмотре демки" +
                            "\n§4/demo stop §7- прекратить смотреть текущую демку"
            );
            return true;
        }

        if (args[0].equalsIgnoreCase("reload")) {
            Main.getInstance().reloadConfig();
            Main.getInstance().reloadConfigParams();
            sender.sendMessage("§eКонфиг перезагружен.");
            return true;
        }

        if (args[0].equalsIgnoreCase("status")) {

            sender.sendMessage("§eStatus: " +
                    "\n  §7Будет ли включаться демка для всех: " + Main.getInstance().isDemoAll() +
                    "\n  §7Игроки, для которых демка будет включаться: " + Main.getInstance().getDemoPlayers() +
                    "\n  §7Игроки, для которых сейчас включена демка: " + Main.getInstance().getDemoRecoders().keySet()
                    .stream().map(Player::getName).collect(Collectors.joining(", "))
            );
            return true;
        }

        if (args[0].equalsIgnoreCase("enable")) {
            if (args.length < 2) {
                sender.sendMessage("§4/demo enable [ник]/all §7- начать записывать демки");
                return true;
            }

            if (args[1].equalsIgnoreCase("all")) {
                Main.getInstance().setDemoAll(true);
                Main.getInstance().getDemoPlayers().clear();
                sender.sendMessage("§aВключаем демку для всех.");
                Main.getInstance().getLogger().info("Включаем демку для всех");

                Bukkit.getScheduler().runTaskAsynchronously(Main.getInstance(), () -> {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        this.startDemo(player, sender);
                    }
                });

                return true;
            }

            if (!Main.getInstance().getDemoPlayers().add(args[1].toLowerCase())) {
                sender.sendMessage("§cИгрок " + args[1] + " уже есть в списке для записи демок.");
            } else {
                Main.getInstance().getLogger().info("Добавили игрока " + args[1] +
                        " в список для демок, текущий список " + Main.getInstance().getDemoPlayers());
                sender.sendMessage("§aВключили запись демки игрока " + args[1] + ".");
            }

            Player player = Bukkit.getPlayer(args[1]);
            if (player != null) {
                Bukkit.getScheduler().runTaskAsynchronously(Main.getInstance(), () -> this.startDemo(player, sender));
            }
            return true;
        }


        if (args[0].equalsIgnoreCase("disable")) {
            if (args.length < 2) {
                sender.sendMessage("§4/demo disable [ник]/all §7- остановить записывать демки");
                return true;
            }

            if (args[1].equalsIgnoreCase("all")) {
                Main.getInstance().setDemoAll(false);
                Main.getInstance().getDemoPlayers().clear();

                sender.sendMessage("§aВыключаем демку для всех.");
                Main.getInstance().getLogger().info("Выключаем демку для всех");

                Bukkit.getScheduler().runTaskAsynchronously(Main.getInstance(), () -> {
                    for (Player player : new ArrayList<>(Main.getInstance().getDemoRecoders().keySet())) {
                        this.stopDemo(player, sender);
                    }
                });
                return true;
            }

            if (!Main.getInstance().getDemoPlayers().remove(args[1].toLowerCase())) {
                sender.sendMessage("§cИгрока " + args[1] + " нет в списке для записи демок.");
            } else {
                sender.sendMessage("§aВыключили запись демки игрока " + args[1] + ".");
                Main.getInstance().getLogger().info("Удалили игрока " + args[1] +
                        " из списока для демок, текущий список " + Main.getInstance().getDemoPlayers());
            }

            Player player = Bukkit.getPlayer(args[1]);
            if (player != null) {
                Bukkit.getScheduler().runTaskAsynchronously(Main.getInstance(), () -> this.stopDemo(player, sender));
            }
            return true;
        }

        if (args[0].equalsIgnoreCase("play")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("§cЭта команда для игрока.");
                return true;
            }

            if (args.length < 2) {
                sender.sendMessage("§4/demo play [имя демки] §7- посмотреть демку");
                return true;
            }

            Player player = (Player) sender;
            if (Main.getInstance().getDemoPlays().containsKey(player)) {
                sender.sendMessage("§cВы уже смотрите демку, сначала остановите предыдущую /demo stop");
                return true;
            }

            File demoFile = new File(Main.getInstance().getDataFolder() + File.separator + "demos", args[1]);
            if (!demoFile.exists()) {
                sender.sendMessage("§cДемка не найдена " + demoFile.getAbsolutePath());
                return true;
            }

            DemoPlay demoPlay = new DemoPlay(player, demoFile);
            Main.getInstance().getDemoPlays().put(player, demoPlay);
            demoPlay.start();
            sender.sendMessage("§aСтарт просмотра демки " + demoFile.getAbsolutePath());
            return true;
        }

        if (args[0].equalsIgnoreCase("stop")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("§cЭта команда для игрока.");
                return true;
            }

            Player player = (Player) sender;
            DemoPlay demoPlay = Main.getInstance().getDemoPlays().remove(player);
            if (demoPlay == null) {
                sender.sendMessage("§cВы не смотрите демку.");
                return true;
            }

            demoPlay.cancel();
            sender.sendMessage("§aОстановка просмотра демки " + demoPlay.getFile().getAbsolutePath());
            return true;
        }

        if (args[0].equalsIgnoreCase("skip")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("§cЭта команда для игрока.");
                return true;
            }

            if (args.length < 2) {
                sender.sendMessage("§4/demo skip [кол-во секунды] §7- промотать вперед при присмотре демки");
                return true;
            }

            Player player = (Player) sender;
            DemoPlay demoPlay = Main.getInstance().getDemoPlays().get(player);
            if (demoPlay == null) {
                sender.sendMessage("§cВы не смотрите демку.");
                return true;
            }


            int seconds;
            try {
                 seconds = Integer.parseInt(args[1]);
            } catch (Exception e) {
                sender.sendMessage("§cНекорректное значение " + args[1] + ".");
                return true;
            }
            if (seconds < 0) {
                seconds = 0;
            }

            sender.sendMessage("§aПроматываем демку на " + seconds + " секунд.");
            demoPlay.skipSeconds(seconds);
            return true;
        }

        sender.sendMessage("§cАргумент команды не найден.");
        return true;
    }

    public void startDemo(Player player, CommandSender sender) {
        DemoRecoder demo = Main.getInstance().getDemoRecoders().get(player);
        if (demo != null) {
            sender.sendMessage("§eУже запущена демка игрока " + player.getName());
            return;
        }

        sender.sendMessage("§eЗапускаем демку игрока " + player.getName());
        try {
            Main.getInstance().startDemo(player);
        } catch (Exception e) {
            sender.sendMessage("§cОшибка включения демки " + player.getName());
            e.printStackTrace();
        }
    }

    public void stopDemo(Player player, CommandSender sender) {
        DemoRecoder demo = Main.getInstance().getDemoRecoders().get(player);
        if (demo == null) {
            sender.sendMessage("§eНет запущенной демки игрока " + player.getName());
            return;
        }

        sender.sendMessage("§eВыключаем демку игрока " + player.getName());
        try {
            Main.getInstance().stopDemo(player);
        } catch (Exception e) {
            sender.sendMessage("§cОшибка выключения демки " + player.getName());
            e.printStackTrace();
        }
    }

    @SuppressWarnings({"LambdaBodyCanBeCodeBlock", "ConstantConditions"})
    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String label, String[] args) {
        if (args.length == 1) {
            return filterTabResponse(Arrays.asList("reload", "enable", "disable", "status", "list", "play", "stop", "skip"), args);
        }


        if (args.length == 2) {
            if (args[0].equals("enable")) {
                List<String> names = Bukkit.getOnlinePlayers()
                        .stream().map(Player::getName).collect(Collectors.toCollection(ArrayList::new));
                names.add("all");
                return filterTabResponse(names, args);
            }
            if (args[0].equals("disable")) {
                List<String> names = new ArrayList<>(Main.getInstance().getDemoPlayers());
                names.add("all");
                return filterTabResponse(names, args);
            }
            if (args[0].equals("play")) {
                File demosDir = new File(Main.getInstance().getDataFolder(), "demos");
                if (demosDir.exists()) {
                    List<String> files = Stream.of(demosDir.listFiles(pathname -> pathname.isFile() &&
                            (pathname.getName().endsWith(".zip") || pathname.getName().endsWith(".dat"))))
                            .map(File::getName)
                            .collect(Collectors.toList());
                    return filterTabResponse(files, args);
                }
            }
        }

        return Collections.emptyList();
    }

    private static List<String> filterTabResponse(List<String> list, String[] args) {
        return list.stream()
                .filter(el -> StringUtils.containsIgnoreCase(el, args[args.length - 1]))
                .collect(Collectors.toList());
    }
}
