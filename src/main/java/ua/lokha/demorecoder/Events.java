package ua.lokha.demorecoder;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class Events implements Listener {

    @EventHandler
    public void on(PlayerJoinEvent event) {
        if (Main.getInstance().isDemoAll()) {
            Main.getInstance().getLogger().info("Включаем демку " + event.getPlayer().getName() +
                    ", потому что указано /demo enable all");
            Bukkit.getScheduler().runTaskAsynchronously(Main.getInstance(),
                    () -> Main.getInstance().startDemo(event.getPlayer()));
        } else if (Main.getInstance().getDemoPlayers().contains(event.getPlayer().getName().toLowerCase())) {
            Main.getInstance().getLogger().info("Включаем демку " + event.getPlayer().getName() +
                    ", потому что указано /demo enable " + event.getPlayer().getName());
            Bukkit.getScheduler().runTaskAsynchronously(Main.getInstance(),
                    () -> Main.getInstance().startDemo(event.getPlayer()));
        }
    }

    @EventHandler
    public void on(PlayerQuitEvent event) {
        if (Main.getInstance().getDemoRecoders().containsKey(event.getPlayer())) {
            Bukkit.getScheduler().runTaskAsynchronously(Main.getInstance(),
                    () -> Main.getInstance().stopDemo(event.getPlayer()));
        }
    }
}
