package ua.lokha.demorecoder;

import io.netty.channel.Channel;
import org.bukkit.entity.Player;

public class BukkitUtils {

    public static Channel getChannel(Player player) {
        return MyObject.wrap(player)
                .invokeMethod("getHandle")
                .getField("playerConnection")
                .getField("networkManager")
                .getField("channel")
                .getObject();
    }
}
