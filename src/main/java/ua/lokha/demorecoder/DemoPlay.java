package ua.lokha.demorecoder;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketContainer;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.commons.io.input.CountingInputStream;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Проигрывание записанной демки
 */
@Getter
public class DemoPlay extends BukkitRunnable {
    /**
     * Кому проигрываем
     */
    private static ProtocolManager manager = ProtocolLibrary.getProtocolManager();
    private static SimpleDateFormat formattedDate = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

    private Player view;
    private Channel channel;
    private ChannelHandler encoder;
    private ChannelHandlerContext context;
    private File file;
    private CountingInputStream countingInputStream;
    private ObjectInputStream inputStream;
    private long totalDemoDataSize;
    private long offsetTime = System.currentTimeMillis();
    private String playerName = "meta.dat is absent";
    private long demoStartTime;
    private long demoEndTime;
    private int sleep = -1;

    @SneakyThrows
    public DemoPlay(Player view, File file) {
        this.view = view;
        this.channel = BukkitUtils.getChannel(view);
        this.encoder = channel.pipeline().get("encoder");
        this.context = channel.pipeline().context(encoder);

        this.file = file;

        if (file.getName().endsWith(".zip")) {
            try (ZipInputStream zip = new ZipInputStream(new FileInputStream(file))) {
                ZipEntry entry;
                while ((entry = zip.getNextEntry()) != null) {
                    if (entry.getName().equals("meta.dat")) {
                        try (ObjectInputStream inputStream = new ObjectInputStream(zip)) {
                            playerName = inputStream.readUTF();
                            demoStartTime = inputStream.readLong();
                            demoEndTime = inputStream.readLong();
                        }
                        break;
                    }
                }
            }
            if (playerName.equals("meta.dat is absent")) {
                Main.getInstance().getLogger().warning("meta.dat in " + file.getName() + " is absent");
            }
            ZipInputStream zip = new ZipInputStream(new FileInputStream(file));
            ZipEntry entry;
            while ((entry = zip.getNextEntry()) != null) {
                if (entry.getName().equals("demo.dat")) {
                    totalDemoDataSize = entry.getSize();
                    countingInputStream = new CountingInputStream(zip);
                    inputStream = new ObjectInputStream(countingInputStream);
                    break;
                }
            }
            if (inputStream == null) {
                zip.close();
                throw new IllegalStateException("demo.dat in " + file.getName() + " is absent");
            }
        } else {
            totalDemoDataSize = file.length();
            countingInputStream = new CountingInputStream(new FileInputStream(file));
            inputStream = new ObjectInputStream(countingInputStream);
        }

        this.updateTitle();

        BukkitUtils.getChannel(view).pipeline().addAfter("decoder", "demo-play", new DemoPlayPacketAdapter());

        if (!view.getAllowFlight()) {
            view.setAllowFlight(true);
        }
        if (!view.isFlying()) {
            view.setFlying(true);
        }
    }

    public void start() {
        this.runTaskTimer(Main.getInstance(), 1, 1);
        Main.getInstance().getLogger().info("Начинаем проигрывать демку " + file.getName() + " для игрока " + view.getName());
        view.sendMessage("§e[Demo] Время начала демки - " + formattedDate.format(new Date(demoStartTime)));
    }

    @Override
    @SneakyThrows
    public void run() {
        try {
            play();
        } catch (Exception e) {
            Main.getInstance().getLogger().info("Ошибка проигрывания демки " + file.getName() + " для игрока " + view.getName());
            e.printStackTrace();
            view.sendMessage("§c[Demo] Ошибка во время проигрывания демки, демка была остановлена. " +
                    "Смотрите консоль для подробностей. " +
                    "Файл демки был прочитан на " +
                    String.format( "%.2f", ((double)countingInputStream.getByteCount() / totalDemoDataSize) * 100) +
            "%.");
            this.cancel();
        }
    }

    @SuppressWarnings("unchecked")
    @SneakyThrows
    public void play() {
        if (!view.isValid()) {
            Main.getInstance().getLogger().warning("Игрок " + view.getName() + " вышел во время просмотра демки " + file.getName()
                    + ", выключаем демку.");
            this.cancel();
            return;
        }

        this.onTick();

        if (sleep > 0) {
            sleep--;
            return;
        }

        byte[] buffer = new byte[1024];
        while (!this.isCancelled()) {
            if (sleep == -1) {
                try {
                    long demoOffsetTime = inputStream.readLong();
                    sleep = (int) ((demoOffsetTime - (System.currentTimeMillis() - offsetTime)) / 50);
                    if (sleep > 0) {
                        return;
                    }
                } catch (EOFException e) {
                    this.cancel();
                    return;
                }
            }
            sleep = -1;

            int action = inputStream.readInt();
            if (action == 0) { // ByteBufPacketAction
                int len = inputStream.readInt();
                ByteBuf buf = channel.alloc().buffer(len);
                int count;
                while ((count = inputStream.read(buffer, 0, Math.min(buffer.length, len - buf.readableBytes()))) > 0) {
                    if (this.isCancelled()) {
                        buf.release();
                        return;
                    }
                    buf.writeBytes(buffer, 0, count);
                }
                int readableBytes = buf.readableBytes();
                if (readableBytes != len) {
                    buf.release();
                    throw new IllegalStateException("пакет записан не полностью, ожидаемая длина " + len +
                            ", реальная длина " + readableBytes);
                }
                context.write(buf, channel.voidPromise());
            } else if (action == 2) {
                view.sendMessage("§e[Demo] " + inputStream.readUTF());
            } else if (action == 1) {
                Map<String, Object> map = (Map<String, Object>) inputStream.readObject();
                if (Bukkit.getWorld((String) map.get("world")) == null) {
                    map.put("world", view.getPlayer().getWorld().getName());
                }
                Location location = Location.deserialize(map);
                view.teleport(location);
            } else if (action == 3) {
                PacketContainer packet = manager.createPacket(PacketType.Play.Server.CLOSE_WINDOW);
                manager.sendServerPacket(view, packet);
            } else {
                throw new IllegalStateException("unknown action " + action);
            }
        }
    }

    @Override
    @SneakyThrows
    public synchronized void cancel() throws IllegalStateException {
        try {
            inputStream.close();
            Main.getInstance().getDemoPlays().remove(view);
            Main.getInstance().getLogger().info("Закончили проигрывать демку " + file.getName() + " для игрока " + view.getName());

            try {
                BukkitUtils.getChannel(view).pipeline().remove("demo-play");
            } catch (Exception ignored){}
            view.sendTitle("", "", 0, 0, 0);
            view.sendMessage("§e[Demo] Закончили проигрывать демку " + file.getName() + ", время - " +
                    formattedDate.format(new Date(demoStartTime + (System.currentTimeMillis() - offsetTime))));
        } finally {
            super.cancel();
        }
    }

    public void skipSeconds(int seconds) {
        offsetTime -= seconds * 1000;
    }

    /**
     * Вызывается примерно раз в тик
     */
    private void onTick() {
        this.updateTitle();
    }

    private void updateTitle() {
        int seconds = (int) ((System.currentTimeMillis() - offsetTime) / 1000);
        int minutes = seconds / 60;
        seconds = seconds - minutes * 60;

        StringBuilder info = new StringBuilder();
        info.append(playerName).append(" - ");

        info.append(minutes).append(":");
        if (seconds < 10) {
            info.append("0");
        }
        info.append(seconds);
        info.append(" / ");

        int fullSeconds = (int) ((demoEndTime - demoStartTime) / 1000);
        int fullMinutes = fullSeconds / 60;
        fullSeconds = fullSeconds - fullMinutes * 60;
        info.append(fullMinutes).append(":");
        if (fullSeconds < 10) {
            info.append("0");
        }
        info.append(fullSeconds);

        view.sendTitle("", info.toString(), 0, 20*5, 10);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DemoPlay demoPlay = (DemoPlay) o;
        return Objects.equals(view, demoPlay.view);
    }

    @Override
    public int hashCode() {
        return Objects.hash(view);
    }

    @Override
    public String toString() {
        return "DemoPlay{" +
                "player=" + view +
                ", file=" + file +
                '}';
    }
}
