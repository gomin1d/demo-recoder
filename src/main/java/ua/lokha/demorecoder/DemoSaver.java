package ua.lokha.demorecoder;

import lombok.SneakyThrows;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class DemoSaver extends Thread {

    public DemoSaver() {
        super("DemoSaver");
    }

    @Override
    @SneakyThrows
    public void run() {
        Main.getInstance().getLogger().info("Поток DemoSaver начал свою работу.");
        while (!this.isInterrupted() && !Main.getInstance().getDemoRecoders().isEmpty()) {
            boolean anyFlush = false;
            List<Player> doRemove = null;
            for (DemoRecoder recoder : Main.getInstance().getDemoRecoders().values()) {
                try {
                    if (!recoder.getChannel().isOpen()) {
                        Main.getInstance().getLogger().info("Игрок " + recoder.getPlayer().getName() +
                                " вышел во время записи демки, останавливаем демку " + recoder.getFile().getName());
                        if (doRemove == null) {
                            doRemove = new ArrayList<>();
                            doRemove.add(recoder.getPlayer());
                            continue;
                        }
                    }

                    if (!recoder.getQueue().isEmpty()) {
                        anyFlush = true;
                        recoder.flushQueue();
                    }
                } catch (Exception e) {
                    Main.getInstance().getLogger().severe("Ошибка в цикле при обработке recoder: " + recoder);
                    e.printStackTrace();
                }
            }
            if (doRemove != null) {
                for (Player player : doRemove) {
                    try {
                        Main.getInstance().stopDemo(player);
                    } catch (Exception e) {
                        Main.getInstance().getLogger().severe("Ошибка в цикле при обработке player: " + player);
                        e.printStackTrace();
                    }
                }
            }
            if (!anyFlush) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    return;
                }
            }
        }

        Main.getInstance().getLogger().info("Поток DemoSaver завершил свою работу.");
    }
}
