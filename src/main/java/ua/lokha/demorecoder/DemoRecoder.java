package ua.lokha.demorecoder;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.entity.Player;
import ua.lokha.demorecoder.action.Action;
import ua.lokha.demorecoder.action.ByteBufPacketAction;
import ua.lokha.demorecoder.action.MoveAction;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Демка, которая записывается
 */
@Getter
public class DemoRecoder extends ChannelOutboundHandlerAdapter {
    private static SimpleDateFormat formattedDate = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");

    private Player player;
    private Channel channel;
    private File file;
    private ObjectOutputStream outputStream;
    private Queue<Action> queue = new ConcurrentLinkedQueue<>();
    private long startTime = System.currentTimeMillis();

    @SneakyThrows
    public DemoRecoder(Player player) {
        this.player = player;
        this.channel = BukkitUtils.getChannel(player);


        file = new File(Main.getInstance().getDataFolder() + File.separator + "demos",
                player.getName() + "-" + formattedDate.format(new Date(System.currentTimeMillis())) + ".dat");
        file.getParentFile().mkdirs();
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        outputStream = new ObjectOutputStream(new FileOutputStream(file));
    }

    public void start() {
        channel.pipeline().addAfter("encoder", "demo-recoder", this);
        queue.add(new MoveAction(player.getLocation()));
    }

    public void stop() throws Exception {
        try {
            channel.pipeline().remove("demo-recoder");
        } catch (NoSuchElementException ignored) {
        }

        this.flushQueue();
        outputStream.flush();
        outputStream.close();

        // add to zip
        File fileZip = new File(file.getParentFile(), StringUtils.removeEnd(file.getName(), ".dat") + ".zip");
        try (ZipOutputStream zip = new ZipOutputStream(new FileOutputStream(fileZip))) {
            zip.putNextEntry(new ZipEntry("demo.dat"));
            try (FileInputStream inputStream = new FileInputStream(file)) {
                byte[] bytes = new byte[1024];
                int length;
                while((length = inputStream.read(bytes)) >= 0) {
                    zip.write(bytes, 0, length);
                }
            }
            zip.flush();

            // записываем метаданные
            zip.putNextEntry(new ZipEntry("meta.dat"));
            ObjectOutputStream metaOut = new ObjectOutputStream(zip);

            metaOut.writeUTF(player.getName());
            metaOut.writeLong(startTime);
            metaOut.writeLong(System.currentTimeMillis());
            metaOut.flush();
        }
        file.delete();
    }

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        try {
            if (msg instanceof ByteBuf) {
                ByteBuf buf = (ByteBuf) msg;
                if (buf.readableBytes() > 0) {
                    int index = buf.readerIndex();
                    int id = PacketUtils.readVarInt(buf);
                    buf.readerIndex(index);

                    if (Main.getInstance().getPacketIds().contains(id)) {
                        queue.add(new ByteBufPacketAction(buf.copy()));
                    }
                }
            }
        } catch (Exception e) {
            Main.getInstance().getLogger().severe("Ошибка обработки пакета " + player.getName());
            e.printStackTrace();
        } finally {
            super.write(ctx, msg, promise);
        }
    }

    @SneakyThrows
    public synchronized void flushQueue() {
        Action action;
        while ((action = queue.poll()) != null) {
            outputStream.writeLong(action.getTime() - startTime);
            action.write(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DemoRecoder demo = (DemoRecoder) o;
        return Objects.equals(player, demo.player);
    }

    @Override
    public int hashCode() {
        return Objects.hash(player);
    }

    @Override
    public String toString() {
        return "Demo{" +
                "player=" + player.getName() +
                '}';
    }
}
