package ua.lokha.demorecoder;

import com.comphenix.protocol.PacketType;
import io.netty.buffer.ByteBuf;
import lombok.Lombok;
import lombok.SneakyThrows;
import org.bukkit.Bukkit;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"ThrowableNotThrown", "CatchMayIgnoreException"})
public class PacketUtils {

    private static MyObject play;
    private static Object direction;

    static {
        try {
            String version = Bukkit.getServer().getClass().getName().split("\\.")[3];
            play = MyObject.wrap(Class.forName("net.minecraft.server." + version + ".EnumProtocol"))
                    .getField("PLAY");
            direction = MyObject.wrap(Class.forName("net.minecraft.server." + version + ".EnumProtocolDirection"))
                    .getField("CLIENTBOUND")
                    .getObject();

        } catch (Exception e) {
            Lombok.sneakyThrow(e);
        }
    }

    private static Map<PacketType, Integer> serverGameIds = new HashMap<>();

    @SneakyThrows
    public static int getServerGameId(PacketType type) {
        Integer id = serverGameIds.get(type);
        if (id == null) {
            id = play.invokeMethod("a", direction, type.getPacketClass().newInstance())
                    .getObject();
            serverGameIds.put(type, id);
        }
        return id;
    }

    public static String dump(ByteBuf buf) {
        buf.markReaderIndex();
        byte[] bytes = new byte[buf.readableBytes()];
        buf.readBytes(bytes);
        String result = Arrays.toString(bytes) + " - " + new String(bytes, StandardCharsets.UTF_8);
        buf.resetReaderIndex();
        return result;
    }

    public static int readVarInt(ByteBuf input) {
        return readVarInt(input, 5);
    }

    public static int readVarInt(ByteBuf input, int maxBytes) {
        int out = 0;
        int bytes = 0;

        byte in;
        do {
            in = input.readByte();
            out |= (in & 127) << bytes++ * 7;
            if (bytes > maxBytes) {
                throw new RuntimeException("VarInt too big");
            }
        } while ((in & 128) == 128);

        return out;
    }

    public static void writeVarInt(int value, ByteBuf output) {
        do {
            int part = value & 127;
            value >>>= 7;
            if (value != 0) {
                part |= 128;
            }

            output.writeByte(part);
        } while (value != 0);

    }

    public static void releaseByteBuf(ByteBuf buf)
    {
        if ( buf != null && buf.refCnt() != 0 )
        {
            while ( buf.refCnt() != 0 )
            {
                buf.release();
            }
        }
    }
}
