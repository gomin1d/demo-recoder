package ua.lokha.demorecoder.action;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import ua.lokha.demorecoder.DemoRecoder;

@AllArgsConstructor
public class InfoAction extends Action {
    private String info;

    @Override
    @SneakyThrows
    public void write(DemoRecoder recoder) {
        recoder.getOutputStream().writeInt(2); // type id ChatAction
        recoder.getOutputStream().writeUTF(info);
    }
}
