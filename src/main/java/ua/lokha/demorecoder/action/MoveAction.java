package ua.lokha.demorecoder.action;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.bukkit.Location;
import ua.lokha.demorecoder.DemoRecoder;

@AllArgsConstructor
public class MoveAction extends Action {
    private Location loc;

    @Override
    @SneakyThrows
    public void write(DemoRecoder recoder) {
        recoder.getOutputStream().writeInt(1); // type id MoveAction
        recoder.getOutputStream().writeObject(loc.serialize());
    }
}
