package ua.lokha.demorecoder.action;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import ua.lokha.demorecoder.DemoRecoder;

@AllArgsConstructor
public class CloseInventoryAction extends Action {

    @Override
    @SneakyThrows
    public void write(DemoRecoder recoder) {
        recoder.getOutputStream().writeInt(3); // CloseInventoryAction
    }
}
