package ua.lokha.demorecoder.action;

import lombok.Getter;
import ua.lokha.demorecoder.DemoRecoder;

public abstract class Action {
    @Getter
    private long time = System.currentTimeMillis();

    public abstract void write(DemoRecoder recoder);
}
