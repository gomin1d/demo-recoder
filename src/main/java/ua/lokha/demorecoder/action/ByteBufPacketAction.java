package ua.lokha.demorecoder.action;

import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import ua.lokha.demorecoder.DemoRecoder;
import ua.lokha.demorecoder.PacketUtils;

@AllArgsConstructor
public class ByteBufPacketAction extends Action {

    private ByteBuf buf;

    @Override
    @SneakyThrows
    public void write(DemoRecoder recoder) {
        recoder.getOutputStream().writeInt(0); // type id ByteBufPacketAction
        recoder.getOutputStream().writeInt(buf.readableBytes());
        buf.readBytes(recoder.getOutputStream(), buf.readableBytes());
        PacketUtils.releaseByteBuf(buf);
    }
}
