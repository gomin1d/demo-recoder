package ua.lokha.demorecoder;

import com.destroystokyo.paper.event.player.PlayerUseUnknownEntityEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import ua.lokha.demorecoder.action.CloseInventoryAction;
import ua.lokha.demorecoder.action.InfoAction;
import ua.lokha.demorecoder.action.MoveAction;

public class DemoRecoderEvents implements Listener {

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void on(PlayerMoveEvent event) {
        DemoRecoder recoder = Main.getInstance().getDemoRecoders().get(event.getPlayer());
        if (recoder != null) {
            recoder.getQueue().add(new MoveAction(event.getTo().clone()));
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void on(AsyncPlayerChatEvent event) {
        DemoRecoder recoder = Main.getInstance().getDemoRecoders().get(event.getPlayer());
        if (recoder != null) {
            recoder.getQueue().add(new InfoAction("Сообщение в чат: " + event.getMessage()));
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void on(PlayerCommandPreprocessEvent event) {
        DemoRecoder recoder = Main.getInstance().getDemoRecoders().get(event.getPlayer());
        if (recoder != null) {
            recoder.getQueue().add(new InfoAction("Пишет команду: " + event.getMessage()));
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void on(InventoryCloseEvent event) {
        DemoRecoder recoder = Main.getInstance().getDemoRecoders().get(event.getPlayer());
        if (recoder != null) {
            recoder.getQueue().add(new CloseInventoryAction());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void on(InventoryClickEvent event) {
        DemoRecoder recoder = Main.getInstance().getDemoRecoders().get(event.getWhoClicked());
        if (recoder != null) {
            Inventory inv = event.getClickedInventory();
            ItemStack item = event.getCurrentItem();
            if (inv != null && item != null) {
                recoder.getQueue().add(new InfoAction("Кликает в инвентаре: " + inv.getTitle() + " -> " + item));
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void on(PlayerInteractEvent event) {
        if(!event.getAction().equals(Action.PHYSICAL)) {
            DemoRecoder recoder = Main.getInstance().getDemoRecoders().get(event.getPlayer());
            if (recoder != null) {
                recoder.getQueue().add(new InfoAction(
                        (event.getAction().name().startsWith("RIGHT") ? "ПКМ" : "ЛКМ") +
                        ", предмет в руке: " + event.getItem()));
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void on(PlayerUseUnknownEntityEvent event) {
        DemoRecoder recoder = Main.getInstance().getDemoRecoders().get(event.getPlayer());
        if (recoder != null) {
            recoder.getQueue().add(new InfoAction("ПКМ по мобу, entity id: " + event.getEntityId()));
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void on(PlayerInteractEntityEvent event) {
        DemoRecoder recoder = Main.getInstance().getDemoRecoders().get(event.getPlayer());
        if (recoder != null) {
            recoder.getQueue().add(new InfoAction("ПКМ по мобу, моб: " + event.getRightClicked()));
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void on(EntityDamageByEntityEvent event) {
        if(event.getDamager() instanceof Player && event.getCause().equals(EntityDamageEvent.DamageCause.ENTITY_ATTACK)) {
            DemoRecoder recoder = Main.getInstance().getDemoRecoders().get(event.getDamager());
            if (recoder != null) {
                recoder.getQueue().add(new InfoAction("ЛКМ моба, моб: " + event.getEntity()));
            }
        }
    }
}
