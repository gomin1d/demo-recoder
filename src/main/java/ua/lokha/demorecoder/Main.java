package ua.lokha.demorecoder;

import com.comphenix.protocol.PacketType;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Главный класс плагина
 */
public class Main extends JavaPlugin {

    @Getter
    private static Main instance;

    public Main() {
        instance = this;
    }

    /**
     * Для каких игроков необходимо включать демки
     */
    @Getter
    private Set<String> demoPlayers;
    /**
     * Нужно ли включать демки для всех игроков
     */
    @Getter
    @Setter
    private boolean demoAll = false;
    @Getter
    private Map<Player, DemoRecoder> demoRecoders = new ConcurrentHashMap<>();
    @Getter
    private Set<Integer> packetIds;
    @Getter
    private Map<Player, DemoPlay> demoPlays = new ConcurrentHashMap<>();
    private final Object lock = new Object();
    private DemoSaver demoSaver;
    private DemoRecoderEvents events;

    @Override
    public void onEnable() {
        this.saveDefaultConfig();
        this.reloadConfigParams();
        Bukkit.getPluginManager().registerEvents(new Events(), this);

        this.getCommand("demo").setExecutor(new DemoCommandExecutor());
    }

    @Override
    public void onDisable() {
        if (!demoRecoders.isEmpty()) {
            this.getLogger().info("Выключаем текущие демки.");
            for (Player player : new ArrayList<>(demoRecoders.keySet())) {
                try {
                    this.stopDemo(player);
                } catch (Exception e) {
                    this.getLogger().severe("Ошибка остановки демки " + player.getName());
                    e.printStackTrace();
                }
            }
        }
    }

    public void reloadConfigParams() {
        this.demoAll = this.getConfig().getBoolean("enable-all");
        this.demoPlayers = this.getConfig().getStringList("enable-players").stream()
                .map(String::toLowerCase)
                .collect(Collectors.toCollection(HashSet::new));
        this.packetIds = this.getConfig().getStringList("packets").stream()
                .map(name -> {
                    try {
                        return PacketType.Play.Server.getInstance().values().stream()
                                .filter(type -> type.name().equals(name))
                                .findFirst().orElse(null);
                    } catch (Exception e) {
                        Main.getInstance().getLogger().severe("Не найден пакет " + name);
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .map(PacketUtils::getServerGameId)
                .collect(Collectors.toCollection(HashSet::new));
    }

    public void startDemo(Player player) {
        synchronized (lock) {
            this.stopDemo(player);

            DemoRecoder demo = new DemoRecoder(player);

            this.getLogger().info("Запускаем демку " + demo.getFile().getAbsolutePath());
            demoRecoders.put(player, demo);

            demo.start();

            if (demoSaver == null || demoSaver.isInterrupted()) {
                this.demoSaver = new DemoSaver();
                this.getLogger().info("Создаем поток DemoSaver.");
                demoSaver.start();
            }
            if (events == null) {
                this.getLogger().info("Регистрируем ивенты.");
                events = new DemoRecoderEvents();
                Bukkit.getPluginManager().registerEvents(events, this);
            }
        }
    }

    @SneakyThrows
    public void stopDemo(Player player) {
        synchronized (lock) {
            DemoRecoder demo = demoRecoders.remove(player);
            if (demo == null) {
                return;
            }

            this.getLogger().info("Выключаем демку " + demo.getFile().getAbsolutePath());
            demo.stop();

            if (demoRecoders.isEmpty()) {
                if (demoSaver != null) {
                    this.getLogger().info("Отключаем поток, потому что уже не осталось демок.");
                    demoSaver.interrupt();
                    demoSaver = null;
                }

                if (events != null) {
                    this.getLogger().info("Удаляем ивенты, потому что уже не осталось демок.");
                    HandlerList.unregisterAll(events);
                    events = null;
                }
            }
        }
    }
}
