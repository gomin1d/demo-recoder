package ua.lokha.demorecoder;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * Пакет движения мешает при просмотре демки, по этому выключаем его во время просмотра демки
 */
public class DemoPlayPacketAdapter extends ChannelInboundHandlerAdapter {
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (!msg.getClass().getName().contains("PacketPlayInFlying")) {
            super.channelRead(ctx, msg);
        }
    }
}
